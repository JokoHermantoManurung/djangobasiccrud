from django import forms
from .models import guestbook

class guestbookForm(forms.ModelForm):
    class Meta:
      model = guestbook
      fields = [
         'nama',
         'alamat',
         'email',
        ]