from django.db import models

# Create your models here.
class guestbook(models.Model):
    nama = models.CharField(max_length=50)
    alamat = models.CharField(max_length=50)
    email = models.CharField(max_length=50)

    def __str__(self):
        return "{}.{}".format(self.id, self.email)
