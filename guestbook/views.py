from django.shortcuts import render, redirect

# Create your views here.
from .models import guestbook
from .forms import guestbookForm

def create(request):
    akun_form = guestbookForm(request.POST or None)

    if request.method == 'POST':
        if akun_form.is_valid():
            akun_form.save()
        return redirect('guestbook:list')    

    context = {
        "page_title":"Tambah akun",
        "akun_form": akun_form,
    }

    return render(request, 'guestbook/create.html', context)

def home(request):
    return render(request,'index.html')

def list(request):
    semua_guest = guestbook.objects.all()

    context = {
        'page_title':'Guest Boook',
        'semua_guest': semua_guest,
    }
    return render(request,'guestbook/list.html', context)

def delete(request, delete_id):
    guestbook.objects.filter(id=delete_id).delete()
    return redirect('guestbook:list')

def update(request, update_id):
    akun_update = guestbook.objects.get(id=update_id)
    data = {
        'nama'  : akun_update.nama,
        'alamat': akun_update.alamat,
        'email' : akun_update.email,
    }
    akun_form = guestbookForm(request.POST or None, initial=data, instance=akun_update)

    if request.method == 'POST':
        if akun_form.is_valid():
            akun_form.save()
        return redirect('guestbook:list')    

    context = {
        "page_title":"Tambah akun",
        "akun_form": akun_form,
    }

    return render(request, 'guestbook/create.html', context)
