from django.conf.urls import url, include
from django.contrib import admin
app_name = 'guestbook'
urlpatterns = [
    url(r'^guestbook/',include(('guestbook.urls','guestbook'), namespace='guestbook')),
    url(r'^admin/', admin.site.urls),
]
